import { TestBed } from '@angular/core/testing';

import { WriteRulesService } from './write-rules.service';

describe('WriteRulesService', () => {
  let service: WriteRulesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WriteRulesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
