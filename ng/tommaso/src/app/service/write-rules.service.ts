import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Observer, catchError, flatMap, fromEvent, map, mergeMap, of } from 'rxjs';
import { v4 as uuidv4 } from 'uuid';

@Injectable({
  providedIn: 'root'
})
export class WriteRulesService {

  constructor(private http: HttpClient,) { }


  create_temporary_tcp_rule(sourceRegexp: string, host: string, port: number): Observable<void> {
    const specName = `tcp_${host}__${port}`.replace(/[^\w]/g, "_");
    var payloadSpec = { "description": `tcp ${host}:${port} (temporary rule)`, "type": "tcp", "args": [host, port.toString()], "timeout": 5000, }
    return this.http.put<void>("/api/dashboard/probe/specs/" + specName, payloadSpec).pipe(
      mergeMap(x => {
        var id = uuidv4();
        var payloadRule = { "disabled": false, "pattern": sourceRegexp, "spec_names": [specName], "expires": Math.ceil(new Date().getTime() / 1000 + 10 * 60) };
        return this.http.put<void>("/api/dashboard/probe/rules/" + id, payloadRule);
      }),
      //      catchError((x, c) => of())
    )
  }

  // 


}
