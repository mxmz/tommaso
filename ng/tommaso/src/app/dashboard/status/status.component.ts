import { Component, OnInit, ViewChild } from '@angular/core';
import { ReadDashboardService } from 'src/app/service/read-dashboard.service';
import { StoredProbeResults } from 'src/app/model/stored-probe-results';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router, } from '@angular/router';
import { Location } from '@angular/common';
import { timer } from 'rxjs';
import { not } from '@angular/compiler/src/output/output_ast';
import { WriteRulesService } from 'src/app/service/write-rules.service';


@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.scss']
})
export class StatusComponent implements OnInit {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  displayedColumns: string[] = ['source', 'target', 'description', 'passed', 'status', 'elapsed', 'comment', 'time'];
  data = new MatTableDataSource<StoredProbeResults>([]);
  loading = 0;
  filter = "";
  hidePassed = false;
  updated = new Date();

  newSourceRegexp = ""
  newTargetHost = ""
  newTargetPort = ""
  refresh: number = 30;


  constructor(private svc: ReadDashboardService, private route: ActivatedRoute, private location: Location, private r: Router, private writeSvc: WriteRulesService) { }

  loadData() {
    this.loading++;
    const filter = this.filter;
    this.svc.getAllResults(filter).subscribe(rs => {
      this.data = new MatTableDataSource<StoredProbeResults>(rs);
      this.data.sort = this.sort;
      this.data.filterPredicate = (data, filter) => (filter == '*' || data.source.includes(filter) || data.args[0].includes(filter) || data.description.includes(filter)) && (!this.hidePassed || !data.pass);
      this.applyFilter();
      this.loading--;
      //this.location.go(`?filter=${filter}`);
      this.updated = new Date();
      this.refresh = rs.length == 0 ? 5 : 30;
    })
  }

  ngOnInit(): void {
    console.log(":StatusComponent: ngOnInit");
    this.route.queryParamMap.subscribe(
      pm => {
        this.filter = pm.get('filter') || "";
        this.hidePassed = pm.get('hidePassed') == "true";
        //this.applyFilter();
        this.loadData();

      });

    const source = timer(1000, 800);
    let filter = this.filter;
    let hidePassed = this.hidePassed;

    const abc = source.subscribe(val => {
      //console.log(val, '-');
      const now = new Date();
      if (!this.loading) {
        var refresh = this.refresh || 30;
        if (this.filter !== filter || this.hidePassed !== hidePassed) {
          filter = this.filter;
          hidePassed = this.hidePassed;
          this.r.navigate([], { queryParams: { filter: filter, hidePassed: hidePassed } })
        } else if (now.getTime() - this.updated.getTime() > refresh * 1000) {
          this.loadData();
        }
      }
    });

  }

  setFilter(event: Event) {
    const filter = (event.target as HTMLInputElement).value.trim();
    if (filter !== this.filter) {
      this.filter = filter;
      this.applyFilter();
    }
  }

  hideFlagChanged() {
    console.log(this.hidePassed);
    //    this.loadData();
  }

  applyFilter() {
    this.data.filter = this.filter == "" ? "*" : this.filter;

  }
  addTemporaryRule(newSourceRegexp: string, newTargetHost: string, newTargePort: string) {
    this.loading++;
    this.writeSvc.create_temporary_tcp_rule(newSourceRegexp, newTargetHost, parseInt(newTargePort)).subscribe(x => {
      console.log("created")
      setTimeout(() => {
        this.r.navigate([], { queryParams: { filter: newTargetHost, hidePassed: false, refresh: 10 } });
        this.loading--;
      }, 1000
      )

    }
    );
  }
}
