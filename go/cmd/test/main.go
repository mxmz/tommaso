package main

import (
	"fmt"
	"time"

	ts "github.com/tevino/tcp-shaker"
)

func main() {
	c := ts.NewChecker()
	timeout := time.Second * 1
	err := c.CheckAddr("127.0.0.1:80", timeout)
	fmt.Println(err)

}
